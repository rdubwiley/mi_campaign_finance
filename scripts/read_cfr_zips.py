from io import BytesIO
from zipfile import ZipFile
from urllib.request import urlopen

from requests_html import HTMLSession
import pandas as pd

def read_url(url, sep="\t", header=True):
    resp = urlopen(url)
    zipfile = ZipFile(BytesIO(resp.read()))
    output = []
    for name in zipfile.namelist():
        if header is None:
            df = pd.read_csv(zipfile.open(name), sep=sep, error_bad_lines=False, encoding='iso-8859-1', header=header, low_memory = False, warn_bad_lines=False)
        else:
            df = pd.read_csv(zipfile.open(name), sep=sep, error_bad_lines=False, encoding='iso-8859-1', low_memory=False, warn_bad_lines=False)
        output.append(df)
    return output

data = read_url('http://miboecfr.nictusa.com/cfr/dumpall/cfrdetail/2018_mi_cfr_receipts.zip')

contribution_columns = [
    'doc_seq_no',
    'page_no',
    'contribution_id',
    'cont_detail_id',
    'doc_stmnt_year',
    'doc_type_desc',
    'com_legal_name',
    'common_name',
    'cfr_com_id',
    'com_type',
    'can_first_name',
    'can_last_name',
    'contribtype',
    'f_name',
    'l_name_or_org',
    'address',
    'city',
    'state',
    'zip',
    'occupation',
    'employer',
    'received_date',
    'amount',
    'aggregate',
    'extra_desc'
]

def main():
    session = HTMLSession()
    base_url = 'http://miboecfr.nictusa.com/cfr/dumpall/cfrdetail/'
    r = session.get(base_url)
    output = {
        'contributions': [],
        'expenditures': [],
        'receipts': []
    }
    for link in r.html.links:
        try:
            if 'expenditures' in link:
                df = read_url(base_url+link)[0]
                output['expenditures'].append(df)
            elif 'contributions' in link:
                if link.split("_")[-1] != "00.zip" and link.split("_")[-1][0] == "0":
                    df = read_url(base_url+link, header=None)[0]
                    df = df.loc[:, range(25)]
                    df = df.reindex(columns = contribution_columns)
                else:
                    df = read_url(base_url+link)[0]
                    df = df.loc[:, contribution_columns]
                output['contributions'].append(df)
            elif 'receipts' in link:
                df = read_url(base_url+link)[0]
                output['receipts'].append(df)
        except Exception as exc:
            print(f"error with {link}")
    output['contributions'] = pd.concat(output['contributions'], join="inner").reset_index(drop=True)
    output['receipts'] = pd.concat(output['receipts'], join="inner").reset_index(drop=True)
    output['expenditures'] = pd.concat(output['expenditures'], join="inner").reset_index(drop=True)
    output['contributions'].to_csv('contributions.csv', index=False, sep="|")
    output['receipts'].to_csv('receipts.csv', index=False, sep="|")
    output['expenditures'].to_csv('expenditures.csv', index=False, sep="|")

if __name__ == "__main__":
    main()
