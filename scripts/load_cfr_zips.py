from odo import odo, drop
from sqlalchemy import create_engine
import pandas as pd

from config import sql_string

def main():
    try:
        drop(sql_string+"::receipts")
    except Exception:
        print("No table receipts not dropping")
    receipts = pd.read_csv('receipts.csv', sep="|",encoding='iso-8859-1')
    odo(receipts, sql_string+"::receipts", sep="|")
    try:
        drop(sql_string+"::expenditures")
    except Exception:
        print("No table expenditures not dropping")
    expenditures = pd.read_csv('expenditures.csv', sep="|",encoding='iso-8859-1')
    expenditures = expenditures.loc[: ,['doc_seq_no', 'expenditure_type', 'gub_account_type', 'gub_elec_type',
       'page_no', 'expense_id', 'detail_id', 'doc_stmnt_year', 'doc_type_desc',
       'com_legal_name', 'common_name', 'cfr_com_id', 'com_type',
       'schedule_desc', 'exp_desc', 'purpose', 'extra_desc', 'f_name',
       'lname_or_org', 'address', 'city', 'state', 'zip', 'exp_date', 'amount',
       'state_loc', 'supp_opp', 'can_or_ballot', 'county',
       'debt_payment', 'vend_name', 'vend_addr', 'vend_city', 'vend_state',
       'vend_zip', 'gotv_ink_ind', 'fundraiser']]
    odo(expenditures, sql_string+"::expenditures", sep="|")
    try:
        drop(sql_string+"::contributions")
    except Exception:
        print("No table contributions to drop")
    contributions = pd.read_csv('contributions.csv', sep="|")
    odo(contributions, sql_string+"::contributions", sep="|")

if __name__ == "__main__":
    main()
