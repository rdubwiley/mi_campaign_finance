from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pymc3 as pm
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sqlalchemy import create_engine

from config import sql_string
from compute_election_analysis import get_office_analysis

donation_query = """
"""

expenditure_query = """
SELECT expense_id,
    doc_stmnt_year,
    common_name,
    cfr_com_id,
    com_type,
    schedule_desc,
    exp_desc,
    purpose,
    extra_desc,
    f_name,
    lname_or_org,
    address,
    city,
    state,
    zip,
    exp_date,
    amount
FROM public.expenditures
WHERE com_type = 'CAN' AND doc_stmnt_year > 2007
"""

def correct_date(x):
    try:
        return datetime.strptime(x, "%m/%d/%Y")
    except Exception:
        return None

def construct_date_year(x):
    try:
        return x.year+1 if (x.year%2) == 1 else x.year
    except Exception:
        return None

def correct_amount(x):
    try:
        return float(x)
    except Exception:
        return None

def get_expenditures():
    engine = create_engine(sql_string)
    expenditures = pd.read_sql(expenditure_query, con = engine)
    expenditures['exp_date'] = expenditures['exp_date'].apply(correct_date)
    expenditures['amount'] = expenditures['amount'].apply(correct_amount)
    expenditures['election_year'] = expenditures['exp_date'].apply(construct_date_year)
    return expenditures

def get_model(data, svi = False):
    model = pm.Model()
    unique_years = data['election_year'].unique()
    n_years = len(unique_years)
    data['election_year_code'] = data['election_year'].apply(lambda x: np.where(unique_years == x)[0][0])
    year_idx = data['election_year_code'].values
    consult_scaler = MinMaxScaler()
    consult_data = data.loc[:, ['consultation_percent']].values
    consult_data = consult_scaler.fit_transform(consult_data)
    mail_scaler = MinMaxScaler()
    mail_data = data.loc[:, ['mailing_percent']].values
    mail_data = mail_scaler.fit_transform(mail_data)
    total_scaler = MinMaxScaler()
    total_data = data.loc[:, ['amount']].values
    total_data = total_scaler.fit_transform(total_data)
    dem_scaler = MinMaxScaler()
    dem_data = data.loc[:, ['previous_dem_competitiveness']].values
    dem_data = dem_scaler.fit_transform(dem_data)

    y_scaler = MinMaxScaler()
    y_data = data.loc[:, ['adjusted_dropoff_ratio']].values
    y_data = y_scaler.fit_transform(y_data)

    year_idx_t = pm.Minibatch(year_idx, 250)
    total_data_t = pm.Minibatch(total_data, 250)
    dem_data_t = pm.Minibatch(dem_data, 250)
    y_data_t = pm.Minibatch(y_data, 250)

    with model:
        a_mu = pm.Normal('a_mu', mu=0, sd=10)
        a_sd = pm.Lognormal('a_sd', mu=0, sd=1)
        a = pm.Normal('a', mu=a_mu, sd=a_sd, shape=n_years)

        b_total_mu = pm.Normal('b_total_mu', mu=0, sd=10)
        b_total_sd = pm.Lognormal('b_total_sd', mu=0, sd=1)
        b_total = pm.Normal('b_total', mu=b_total_mu, sd=b_total_sd, shape=n_years)

        b_dem_mu = pm.Normal('b_dem_mu', mu=0, sd=10)
        b_dem_sd = pm.Lognormal('b_dem_sd', mu=0, sd=1)
        b_dem = pm.Normal('b_dem', mu=b_dem_mu, sd=b_dem_sd, shape=n_years)

        lin = b_total[year_idx_t]*total_data_t.transpose() + b_dem[year_idx_t]*dem_data_t.transpose() + a[year_idx_t]
        y = pm.Normal('y', mu=lin, sd=1, observed = y_data_t.transpose())
        if svi:
            inference = pm.ADVI()
            approx = pm.fit(n=250000, method=inference)
            trace = approx.sample(10000)
        else:
            trace = pm.sample(50000, tune=10000)
    return trace

def main():
    office_analysis = get_office_analysis()
    office_analysis = office_analysis.iloc[(office_analysis['election_year']>2010).values]
    state_legislature = office_analysis.iloc[(office_analysis['office_code'] > 7).values]
    state_averages = state_legislature.groupby(['election_year','office_code'])['dropoff_ratio'].mean()
    state_legislature['greater_dropoff'] = state_legislature.apply(
        lambda x: 1 if x['dropoff_ratio']>state_averages[x['election_year']][x['office_code']] else 0,
        axis = 1
    )
    state_legislature['adjusted_dropoff_ratio'] = state_legislature.apply(
        lambda x: x['dropoff_ratio']-state_averages[x['election_year']][x['office_code']],
        axis = 1
    )
    expenditures = get_expenditures()
    expenditures['consultation_dollars'] = expenditures.apply(
        lambda x: x['amount'] if x['exp_desc'] == 'CONSULTATION, RESEARCH' else 0,
        axis = 1
    )
    expenditures['mailing_dollars'] = expenditures.apply(
        lambda x: x['amount'] if x['exp_desc'] == 'MAILING,POSTAGE,BULK RATE' else 0,
        axis = 1
    )
    sum_exp = expenditures.groupby(['election_year','cfr_com_id'])['amount','consultation_dollars','mailing_dollars'].sum().reset_index()
    exp_joined = pd.merge(state_legislature, sum_exp, left_on=['election_year','candidate_id'], right_on=['election_year','cfr_com_id'])
    exp_joined['consultation_percent'] = exp_joined['consultation_dollars']/exp_joined['amount']
    exp_joined['mailing_percent'] = exp_joined['mailing_dollars']/exp_joined['amount']
    exp_joined['previous_dem_percent'] = exp_joined['previous_dem_votes']/exp_joined['previous_turnout']
    exp_joined['previous_dem_competitiveness'] = .25-((exp_joined['previous_dem_percent']-.5)**2)
    trace = get_model(exp_joined, svi=True)
    traceplot = pm.traceplot(trace)
    plt.show()

if __name__ == "__main__":
    main()
