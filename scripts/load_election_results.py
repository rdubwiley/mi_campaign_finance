from sqlalchemy import create_engine
import pandas as pd

from config import sql_string

def main():
    engine = create_engine(sql_string)
    print("Loading Cities")
    city = pd.read_csv('city.csv', sep="|")
    city.to_sql('cities', engine, if_exists='replace')
    print("Loading Counties")
    county = pd.read_csv('county.csv', sep="|")
    county.to_sql('counties', engine, if_exists='replace')
    print("Loading Names")
    name = pd.read_csv('name.csv', sep="|")
    name.to_sql('names', engine, if_exists='replace')
    print("Loading Offices")
    offices = pd.read_csv('offices.csv', sep="|")
    offices.to_sql('offices', engine, if_exists='replace')
    print("Loading Votes")
    vote = pd.read_csv('vote.csv', sep="|")
    vote.to_sql('votes', engine, if_exists='replace')

if __name__ == "__main__":
    main()
