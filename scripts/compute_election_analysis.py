from sqlalchemy import create_engine
import pandas as pd

from config import sql_string

precinct_query  = """
SELECT
    election_year,
    election_type,
    office_code,
    district_code,
    status_code,
    candidate_id,
    county_code,
    city_code,
    ward_number,
    precinct_number,
    SUM(precinct_votes) AS dem_votes
FROM (
    SELECT * FROM (
        SELECT election_year,
            election_type,
            office_code,
            district_code,
            status_code,
            candidate_id,
            county_code,
            city_code,
            ward_number,
            precinct_number,
            precinct_votes
            FROM public.votes
        WHERE office_code < 9
    ) AS t1
    JOIN (
        SELECT election_year AS election_year2,
            office_code AS office_code2,
            candidate_id AS candidate_id2
            FROM public.names
            WHERE candidate_party_name='DEM'
    ) AS t2
    ON t1.election_year = t2.election_year2 AND t1.candidate_id = t2.candidate_id2
) AS j1
GROUP BY 1,2,3,4,5,6,7,8,9,10
"""

total_query = """
SELECT election_year,
    election_type,
    office_code,
    district_code,
    status_code,
    county_code,
    city_code,
    ward_number,
    precinct_number,
    SUM(precinct_votes) AS total_votes
	FROM public.votes
	WHERE candidate_id != 0 AND office_code != 0 AND office_code < 9
	GROUP BY 1,2,3,4,5,6,7,8,9
"""

office_query = """
SELECT election_year, office_code, district_code, office_description
	FROM public.offices
	WHERE status_code = 0
	GROUP BY election_year, office_code, district_code, office_description
"""

county_query = """
SELECT county_code, county_name
	FROM public.counties
	GROUP BY 1,2;
"""

city_query = """
SELECT county_code, city_code, city_description
	FROM public.cities
	GROUP BY 1,2,3;
"""

candidate_query = """
SELECT election_year, office_code, district_code, candidate_id
	FROM public.names
	WHERE candidate_party_name = 'DEM'
    GROUP BY 1,2,3,4
"""

def get_aggregated_data():
    engine = create_engine(sql_string)
    precinct = pd.read_sql(precinct_query, engine)
    total_votes = pd.read_sql(total_query, engine)
    merged = pd.merge(precinct, total_votes, on=[
        'election_year',
        'election_type',
        'office_code',
        'district_code',
        'status_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])
    merged['presidential_year'] = merged['election_year'].apply(lambda x: 1 if x%4 == 0 else 0)
    return merged

def aggregate_to_office(data):
    office_aggregation = data.groupby([
        'election_year',
        'office_code',
        'district_code',
    ])['dem_votes','total_votes'].apply(lambda x: x.sum()).reset_index()
    office_aggregation['dem_percent'] = office_aggregation['dem_votes']/office_aggregation['total_votes']
    return office_aggregation

def get_office_chronology(data):
    office_aggregated = aggregate_to_office(data)
    office_aggregated['summary'] = office_aggregated.apply(
        lambda x: {
            'election_year': x['election_year'],
            'dem_votes': x['dem_votes'],
            'turnout': x['total_votes']
        },
        axis = 1
    )
    office_aggregated = office_aggregated.sort_values(['election_year','office_code','district_code'])
    summary = office_aggregated.groupby([
        'office_code',
        'district_code'
    ])['summary'].apply(list).reset_index()
    summary['grouped'] = summary['summary'].apply(
        lambda x: None if len(x) == 1 else [x for x in zip(x,x[1:])]
    )
    summary['grouped2'] = summary['summary'].apply(
        lambda x: None if len(x) < 3 else [x for x in zip(x,x[2:])]
    )
    summary['previous'] = summary.apply(
        lambda x: None if x['grouped'] is None else (
            [
                {
                    'election_year': item[1]['election_year'],
                    'office_code': x['office_code'],
                    'district_code': x['district_code'],
                    'previous_dem_votes': item[0]['dem_votes'],
                    'previous_turnout': item[0]['turnout']
                } for item in x['grouped']
            ]
        ), axis = 1
    )
    summary['previous_two'] = summary.apply(
        lambda x: None if x['grouped2'] is None else (
            [
                {
                    'election_year': item[1]['election_year'],
                    'office_code': x['office_code'],
                    'district_code': x['district_code'],
                    'previous_two_dem_votes': item[0]['dem_votes'],
                    'previous_two_turnout': item[0]['turnout']
                } for item in x['grouped2']
            ]
        ), axis = 1
    )
    summary['next'] = summary.apply(
        lambda x: None if x['grouped'] is None else (
            [
                {
                    'election_year': item[0]['election_year'],
                    'office_code': x['office_code'],
                    'district_code': x['district_code'],
                    'next_dem_votes': item[1]['dem_votes'],
                    'next_turnout': item[1]['turnout']
                } for item in x['grouped']
            ]
        ), axis = 1
    )
    summary['next_two'] = summary.apply(
        lambda x: None if x['grouped2'] is None else (
            [
                {
                    'election_year': item[0]['election_year'],
                    'office_code': x['office_code'],
                    'district_code': x['district_code'],
                    'next_two_dem_votes': item[1]['dem_votes'],
                    'next_two_turnout': item[1]['turnout']
                } for item in x['grouped2']
            ]
        ), axis = 1
    )
    previous_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['previous'] if sub_list is not None for x in sub_list]
    )
    previous_two_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['previous_two'] if sub_list is not None for x in sub_list]
    )
    next_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['next'] if sub_list is not None for x in sub_list]
    )
    next_two_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['next_two'] if sub_list is not None for x in sub_list]
    )
    output = office_aggregated.copy()
    output = pd.merge(output, previous_df, on = [
        'election_year',
        'office_code',
        'district_code'
    ],how="left")
    output = pd.merge(output, previous_two_df, on = [
        'election_year',
        'office_code',
        'district_code'
    ],how="left")
    output = pd.merge(output, next_df, on = [
        'election_year',
        'office_code',
        'district_code'
    ],how="left")
    output = pd.merge(output, next_two_df, on = [
        'election_year',
        'office_code',
        'district_code'
    ],how="left")
    output['previous_dem_ratio'] = output.apply(
        lambda x: None if x['previous_dem_votes'] is None or x['previous_dem_votes'] == 0 else x['dem_votes']/x['previous_dem_votes'],
        axis = 1
    )
    output['prevous_two_dem_ratio'] = output.apply(
        lambda x: None if x['previous_two_dem_votes'] is None or x['previous_two_dem_votes'] == 0 else x['dem_votes']/x['previous_two_dem_votes'],
        axis = 1
    )
    output['next_dem_ratio'] = output.apply(
        lambda x: None if x['next_dem_votes'] is None or x['next_dem_votes'] == 0 else x['dem_votes']/x['next_dem_votes'],
        axis = 1
    )
    output['next_two_dem_ratio'] = output.apply(
        lambda x: None if x['next_two_dem_votes'] is None or x['next_two_dem_votes'] == 0 else x['dem_votes']/x['next_two_dem_votes'],
        axis = 1
    )
    output['previous_turnout_ratio'] =  output.apply(
        lambda x: None if x['previous_turnout'] is None or x['previous_turnout'] == 0 else x['total_votes']/x['previous_turnout'],
        axis = 1
    )
    output['previous_two_turnout_ratio'] =  output.apply(
        lambda x: None if x['previous_two_turnout'] is None or x['previous_two_turnout'] == 0 else x['total_votes']/x['previous_two_turnout'],
        axis = 1
    )
    output['next_turnout_ratio'] =  output.apply(
        lambda x: None if x['next_turnout'] is None or x['next_turnout'] == 0 else x['total_votes']/x['next_turnout'],
        axis = 1
    )
    output['next_two_turout_ratio'] =  output.apply(
        lambda x: None if x['next_two_turnout'] is None or x['next_two_turnout'] == 0 else x['total_votes']/x['next_two_turnout'],
        axis = 1
    )
    del output['summary']
    return output

def get_precinct_chronology(data):
    data['dem_percent'] = data['dem_votes']/data['total_votes']
    data['summary'] = data.apply(
        lambda x: {
            'election_year': x['election_year'],
            'dem_votes': x['dem_votes'],
            'turnout': x['total_votes']
        },
        axis = 1
    )
    summary = data.groupby([
        'office_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])['summary'].apply(list).reset_index()
    summary['grouped'] = summary['summary'].apply(
        lambda x: None if len(x) == 1 else [x for x in zip(x,x[1:])]
    )
    summary['grouped2'] = summary['summary'].apply(
        lambda x: None if len(x) < 3 else [x for x in zip(x,x[2:])]
    )
    summary['previous'] = summary.apply(
        lambda x: None if x['grouped'] is None else (
            [
                {
                    'election_year': item[1]['election_year'],
                    'office_code': x['office_code'],
                    'county_code': x['county_code'],
                    'city_code': x['city_code'],
                    'ward_number': x['ward_number'],
                    'precinct_number': x['precinct_number'],
                    'previous_dem_votes': item[0]['dem_votes'],
                    'previous_turnout': item[0]['turnout']
                } for item in x['grouped']
            ]
        ), axis = 1
    )
    summary['previous_two'] = summary.apply(
        lambda x: None if x['grouped2'] is None else (
            [
                {
                    'election_year': item[1]['election_year'],
                    'office_code': x['office_code'],
                    'county_code': x['county_code'],
                    'city_code': x['city_code'],
                    'ward_number': x['ward_number'],
                    'precinct_number': x['precinct_number'],
                    'previous_two_dem_votes': item[0]['dem_votes'],
                    'previous_two_turnout': item[0]['turnout']
                } for item in x['grouped2']
            ]
        ), axis = 1
    )
    summary['next'] = summary.apply(
        lambda x: None if x['grouped'] is None else (
            [
                {
                    'election_year': item[0]['election_year'],
                    'office_code': x['office_code'],
                    'county_code': x['county_code'],
                    'city_code': x['city_code'],
                    'ward_number': x['ward_number'],
                    'precinct_number': x['precinct_number'],
                    'next_dem_votes': item[1]['dem_votes'],
                    'next_turnout': item[1]['turnout']
                } for item in x['grouped']
            ]
        ), axis = 1
    )
    summary['next_two'] = summary.apply(
        lambda x: None if x['grouped2'] is None else (
            [
                {
                    'election_year': item[0]['election_year'],
                    'office_code': x['office_code'],
                    'county_code': x['county_code'],
                    'city_code': x['city_code'],
                    'ward_number': x['ward_number'],
                    'precinct_number': x['precinct_number'],
                    'next_two_dem_votes': item[1]['dem_votes'],
                    'next_two_turnout': item[1]['turnout']
                } for item in x['grouped2']
            ]
        ), axis = 1
    )
    previous_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['previous'] if sub_list is not None for x in sub_list]
    )
    previous_two_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['previous_two'] if sub_list is not None for x in sub_list]
    )
    next_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['next'] if sub_list is not None for x in sub_list]
    )
    next_two_df = pd.DataFrame.from_dict(
        [ x for sub_list in summary['next_two'] if sub_list is not None for x in sub_list]
    )
    output = data.copy()
    output = pd.merge(output, previous_df, on = [
        'election_year',
        'office_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ],how="left")
    output = pd.merge(output, previous_two_df, on = [
        'election_year',
        'office_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ],how="left")
    output = pd.merge(output, next_df, on = [
        'election_year',
        'office_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ],how="left")
    output = pd.merge(output, next_two_df, on = [
        'election_year',
        'office_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ],how="left")
    output['previous_dem_ratio'] = output.apply(
        lambda x: None if x['previous_dem_votes'] is None or x['previous_dem_votes'] == 0 else x['dem_votes']/x['previous_dem_votes'],
        axis = 1
    )
    output['prevous_two_dem_ratio'] = output.apply(
        lambda x: None if x['previous_two_dem_votes'] is None or x['previous_two_dem_votes'] == 0 else x['dem_votes']/x['previous_two_dem_votes'],
        axis = 1
    )
    output['next_dem_ratio'] = output.apply(
        lambda x: None if x['next_dem_votes'] is None or x['next_dem_votes'] == 0 else x['dem_votes']/x['next_dem_votes'],
        axis = 1
    )
    output['next_two_dem_ratio'] = output.apply(
        lambda x: None if x['next_two_dem_votes'] is None or x['next_two_dem_votes'] == 0 else x['dem_votes']/x['next_two_dem_votes'],
        axis = 1
    )
    output['previous_turnout_ratio'] =  output.apply(
        lambda x: None if x['previous_turnout'] is None or x['previous_turnout'] == 0 else x['total_votes']/x['previous_turnout'],
        axis = 1
    )
    output['previous_two_turnout_ratio'] =  output.apply(
        lambda x: None if x['previous_two_turnout'] is None or x['previous_two_turnout'] == 0 else x['total_votes']/x['previous_two_turnout'],
        axis = 1
    )
    output['next_turnout_ratio'] =  output.apply(
        lambda x: None if x['next_turnout'] is None or x['next_turnout'] == 0 else x['total_votes']/x['next_turnout'],
        axis = 1
    )
    output['next_two_turout_ratio'] =  output.apply(
        lambda x: None if x['next_two_turnout'] is None or x['next_two_turnout'] == 0 else x['total_votes']/x['next_two_turnout'],
        axis = 1
    )
    del output['summary']
    return output

def get_precinct_averages(data):
    data['dem_percent'] = data['dem_votes']/data['total_votes']
    summary = data.groupby([
        'election_year',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])['dem_percent'].mean().reset_index()
    return summary

def get_highest_turnout(data):
    summary = data.groupby([
        'election_year',
        'status_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])['total_votes'].apply(lambda x: list(x)[0]).reset_index()
    cols = list(summary.columns)
    cols[-1] = 'highest_total_votes'
    summary.columns = cols
    return summary

def get_highest_dem(data):
    summary = data.groupby([
        'election_year',
        'status_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])['dem_votes'].apply(lambda x: list(x)[0]).reset_index()
    cols = list(summary.columns)
    cols[-1] = 'highest_dem_votes'
    summary.columns = cols
    return summary

def compute_down_ballots(data):
    df = data
    data['dem_percent'] = data['dem_votes']/data['total_votes']
    highest_turnout = get_highest_turnout(data)
    highest_dem = get_highest_dem(data)
    df = pd.merge(df, highest_turnout, on = [
        'election_year',
        'status_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])
    df = pd.merge(df, highest_dem, on = [
        'election_year',
        'status_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])
    df['total_dt_dropoff'] = df['total_votes']/df['highest_total_votes']
    df['dem_dt_dropoff'] = df['dem_votes']/df['highest_dem_votes']
    df['dropoff_ratio'] = df['dem_dt_dropoff']/df['total_dt_dropoff']
    return df

def get_down_ballot_by_office(data):
    down_ballots = compute_down_ballots(data)
    summary = down_ballots.groupby([
        'election_year',
        'office_code',
        'district_code'
    ])['highest_total_votes','total_votes','highest_dem_votes','dem_votes'].apply(lambda x: x.sum()).reset_index()
    summary['total_dt_dropoff'] = summary['total_votes']/summary['highest_total_votes']
    summary['dem_dt_dropoff'] = summary['dem_votes']/summary['highest_dem_votes']
    summary['dropoff_ratio'] = summary['dem_dt_dropoff']/summary['total_dt_dropoff']
    return summary

def correct_columns(df):
    columns = df.columns
    corrected_columns = [ x for x in columns if "_y" not in x[-2:]]
    output = df.loc[:, corrected_columns]
    renamed_columns = [ x.replace("_x", "") for x in corrected_columns ]
    output.columns = renamed_columns
    return output

def get_offices():
    engine = create_engine(sql_string)
    offices = pd.read_sql(office_query, engine)
    return offices

def get_counties():
    engine = create_engine(sql_string)
    counties = pd.read_sql(county_query, engine)
    return counties

def get_cities():
    engine = create_engine(sql_string)
    cities = pd.read_sql(city_query, engine)
    return cities

def get_candidates():
    engine = create_engine(sql_string)
    candidates = pd.read_sql(candidate_query, engine)
    return candidates

def get_precinct_analysis():
    data = get_aggregated_data()
    precinct_chronology = get_precinct_chronology(data)
    precinct_down_ballots = compute_down_ballots(data)
    precinct_analysis = pd.merge(precinct_chronology, precinct_down_ballots, on = [
        'election_year',
        'office_code',
        'district_code',
        'status_code',
        'county_code',
        'city_code',
        'ward_number',
        'precinct_number'
    ])
    precinct_analysis = correct_columns(precinct_analysis)
    offices = get_offices()
    counties = get_counties()
    cities = get_cities()
    precinct_analysis = pd.merge(precinct_analysis, offices, on = [
        'office_code',
        'district_code'
    ], how="left")
    precinct_analysis = pd.merge(precinct_analysis, counties, on = [
        'county_code'
    ], how="left")
    precinct_analysis = pd.merge(precinct_analysis, cities, on = [
        'county_code',
        'city_code'
    ], how="left")
    precinct_analysis = precinct_analysis.loc[
        :,
        [
           'election_year', 'office_description', 'county_name', 'city_description',
           'ward_number', 'precinct_number','election_type', 'office_code', 'district_code',
           'status_code', 'candidate_id', 'county_code', 'city_code',
           'dem_votes', 'total_votes',
           'presidential_year', 'dem_percent', 'previous_dem_votes',
           'previous_turnout', 'previous_two_dem_votes', 'previous_two_turnout',
           'next_dem_votes', 'next_turnout', 'next_two_dem_votes',
           'next_two_turnout', 'previous_dem_ratio', 'prevous_two_dem_ratio',
           'next_dem_ratio', 'next_two_dem_ratio', 'previous_turnout_ratio',
           'previous_two_turnout_ratio', 'next_turnout_ratio',
           'next_two_turout_ratio', 'highest_total_votes', 'highest_dem_votes',
           'total_dt_dropoff', 'dem_dt_dropoff', 'dropoff_ratio',
       ]
    ]
    return precinct_analysis

def get_office_analysis():
    data = get_aggregated_data()
    office_chronology = get_office_chronology(data)
    office_down_ballots = get_down_ballot_by_office(data)
    office_analysis = pd.merge(office_chronology, office_down_ballots, on = [
        'election_year',
        'office_code',
        'district_code'
    ])
    office_analysis = correct_columns(office_analysis)
    offices = get_offices()
    candidates = get_candidates()
    office_analysis = pd.merge(office_analysis, offices, on = [
        'election_year',
        'office_code',
        'district_code'
    ], how="left")
    office_analysis = pd.merge(office_analysis, candidates, on = [
        'election_year',
        'office_code',
        'district_code'
    ])
    office_analysis = office_analysis.loc[
        :,
        [
           'election_year', 'office_description','office_code', 'district_code', 'candidate_id', 'dem_votes',
           'total_votes', 'dem_percent', 'previous_dem_votes', 'previous_turnout',
           'previous_two_dem_votes', 'previous_two_turnout', 'next_dem_votes',
           'next_turnout', 'next_two_dem_votes', 'next_two_turnout',
           'previous_dem_ratio', 'prevous_two_dem_ratio', 'next_dem_ratio',
           'next_two_dem_ratio', 'previous_turnout_ratio',
           'previous_two_turnout_ratio', 'next_turnout_ratio',
           'next_two_turout_ratio', 'highest_total_votes', 'highest_dem_votes',
           'total_dt_dropoff', 'dem_dt_dropoff', 'dropoff_ratio',

       ]
    ]
    return office_analysis

def main():
    precinct_analysis = get_precinct_analysis()
    precinct_analysis.to_csv('precinct_analysis.csv', index=False)
    office_analysis = get_office_analysis()
    office_analysis.to_csv('office_analysis.csv', index=False)

if __name__ == "__main__":
    main()
