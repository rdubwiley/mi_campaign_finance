from datetime import datetime

import numpy as np
import pandas as pd
from sqlalchemy import create_engine

from config import sql_string
from compute_election_analysis import get_office_analysis

expenditure_query = """
SELECT expense_id,
    doc_stmnt_year,
    common_name,
    cfr_com_id,
    com_type,
    schedule_desc,
    exp_desc,
    purpose,
    extra_desc,
    f_name,
    lname_or_org,
    address,
    city,
    state,
    zip,
    exp_date,
    amount
FROM public.expenditures
WHERE com_type = 'CAN' AND doc_stmnt_year > 2007
"""

def correct_date(x):
    try:
        return datetime.strptime(x, "%m/%d/%Y")
    except Exception:
        return None

def construct_date_year(x):
    try:
        return x.year+1 if (x.year%2) == 1 else x.year
    except Exception:
        return None

def correct_amount(x):
    try:
        return float(x)
    except Exception:
        return None

def get_cfr_data():
    """
    Grabs the CFR data. You will need to do some groupbys as this
    needs to join to the election results.
    """
    engine = create_engine(sql_string)
    expenditures = pd.read_sql(expenditure_query, con = engine)
    #TODO: Enter your additional transformations
    return expenditures

def compute_transormations(data):
    """
    This is a function
    """

def get_model(data):
    """
    This is what you need to build to model the dropoff ratio
    """

def main():
    office_analysis = get_office_analysis()
    office_analysis = office_analysis.iloc[(office_analysis['election_year']>2010).values]
    state_legislature = office_analysis.iloc[(office_analysis['office_code'] > 7).values]
    state_averages = state_legislature.groupby(['election_year','office_code'])['dropoff_ratio'].mean()
    state_legislature['greater_dropoff'] = state_legislature.apply(
        lambda x: 1 if x['dropoff_ratio']>state_averages[x['election_year']][x['office_code']] else 0,
        axis = 1
    )
    state_legislature['adjusted_dropoff_ratio'] = state_legislature.apply(
        lambda x: x['dropoff_ratio']-state_averages[x['election_year']][x['office_code']],
        axis = 1
    )
    cfr_data = get_cfr_data()
    joined = pd.merge(state_legislature, sum_exp, left_on=['election_year','candidate_id'], right_on=['election_year','cfr_com_id'])
    joined = compute_transformations(joined)
    model = get_model(joined)

if __name__ == "__main__":
    main()
