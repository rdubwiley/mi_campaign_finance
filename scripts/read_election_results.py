from io import BytesIO
from zipfile import ZipFile
from urllib.request import urlopen

from requests_html import HTMLSession
import pandas as pd

county_columns = [
    'county_code',
    'county_name'
]

office_columns = [
    'election_year',
    'election_type',
    'office_code',
    'district_code',
    'status_code',
    'office_description'
]

vote_columns = [
    'election_year',
    'election_type',
    'office_code',
    'district_code',
    'status_code',
    'candidate_id',
    'county_code',
    'city_code',
    'ward_number',
    'precinct_number',
    'precinct_label',
    'precinct_votes'
]

name_columns = [
    'election_year',
    'election_type',
    'office_code',
    'district_code',
    'status_code',
    'candidate_id',
    'candidate_last_name',
    'candidate_first_name',
    'candidate_middle_name',
    'candidate_party_name'
]

city_columns = [
    'election_year',
    'election_type',
    'county_code',
    'city_code',
    'city_description'
]

def read_election_url(url, sep="\t", header=True):
    resp = urlopen(url)
    zipfile = ZipFile(BytesIO(resp.read()))
    output = {
        'county': None,
        'vote': None,
        'offices': None,
        'name': None,
        'city': None
    }
    for name in zipfile.namelist():
        if 'readme' not in name:
            df = pd.read_csv(zipfile.open(name), sep=sep, header=header)
            df = df.loc[:, list(df.columns.values)[:-1]]
        if 'county' in name:
            df.columns = county_columns
            output['county'] = df
        if 'offc' in name:
            df.columns = office_columns
            output['offices'] = df
        if 'vote' in name:
            df.columns = vote_columns
            output['vote'] = df
        if 'name' in name:
            df.columns = name_columns
            output['name'] = df
        if 'city' in name:
            df.columns = city_columns
            output['city'] = df
    return output


def read_year(year):
    url = f'http://miboecfr.nictusa.com/cfr/presults/{year}GEN.zip'
    data = read_election_url(url, header=None)
    return data


def read_election_results(years):
    output = {
        'county': [],
        'vote': [],
        'offices': [],
        'name': [],
        'city': []
    }
    for year in years:
        data = read_year(year)
        for key in output:
            output[key].append(data[key])
    for key in output:
        output[key] = pd.concat(output[key], join="inner").reset_index(drop=True)
    return output


years = [
    2008,
    2010,
    2012,
    2014,
    2016
]
data = read_election_results(years)

for key in data:
    data[key].to_csv(f'{key}.csv', index=False, sep="|")
